package hratos.praksa.mateojovic.zadatakak15;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ZadaciBaza {

    @Query("Select * from Zadaci")
    LiveData<List<Zadaci>> getZadaciList();

    @Insert
    void insertZadaci(Zadaci zadaci);

    @Update
    void updateZadaci(Zadaci zadaci);

    @Delete
    void deleteZadaci(Zadaci zadaci);

}

