package hratos.praksa.mateojovic.zadatakak15;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ZaposleniciBaza {

    @Query("Select * from Zaposlenici")
    LiveData<List<Zaposlenici>> getZaposleniciList();

    @Insert
    void insertZaposlenik(Zaposlenici zaposlenici);

    @Update
    void updateZaposlenik(Zaposlenici zaposlenici);

    @Delete
    void deleteZaposlenik(Zaposlenici zaposlenici);
}
