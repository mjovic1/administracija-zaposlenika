package hratos.praksa.mateojovic.zadatakak15;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Zaposlenici")
public class Zaposlenici {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "Ime")
    private String ime;
    @ColumnInfo(name = "Prezime")
    private String prezime;
    @ColumnInfo(name = "Radno Mjesto")
    private String radnoMjesto;
    @ColumnInfo(name = "OIB")
    private int oib;

    public Zaposlenici(int id, String ime, String prezime, String radnoMjesto, int oib){
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.radnoMjesto = radnoMjesto;
        this.oib = oib;
    }

    public int getId(){
        return id;
    }

    public String getIme(){
        return ime;
    }

    public String getPrezime(){
        return prezime;
    }

    public String getRadnoMjesto(){
        return radnoMjesto;
    }

    public int getOib(){
        return oib;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setIme(String ime){
        this.ime = ime;
    }

    public void setPrezime(String prezime){
        this.prezime = prezime;
    }

    public void setRadnoMjesto(String radnoMjesto){
        this.radnoMjesto = radnoMjesto;
    }

    public void setOib(int oib){
        this.oib = oib;
    }

}
