package hratos.praksa.mateojovic.zadatakak15;


import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Date;

@Database(entities = {Zadaci.class, Zaposlenici.class}, exportSchema = false, version = 2)
public abstract class KreiranjeBaze extends RoomDatabase {
    private static final String DB_NAME = "Firma";
    private static KreiranjeBaze instance;

    public abstract ZaposleniciBaza zaposleniciBaza();
    public abstract ZadaciBaza zadaciBaza();

    public static synchronized KreiranjeBaze getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), KreiranjeBaze.class, DB_NAME).
                    fallbackToDestructiveMigration().
                    addCallback(roomCallback).
                    build();
        }
        return  instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopuniDbAsycTask(instance).execute();
        }
    };

    private static class PopuniDbAsycTask extends AsyncTask<Void, Void, Void> {
        private ZadaciBaza zadaciBaza;
        private ZaposleniciBaza zaposleniciBaza;

        private PopuniDbAsycTask(KreiranjeBaze db){
            zadaciBaza = db.zadaciBaza();
            zaposleniciBaza = db.zaposleniciBaza();
        }

        String pocetnoVrijeme = java.text.DateFormat.getDateTimeInstance().format(new Date());
        String zavrsenoVrijeme = java.text.DateFormat.getDateTimeInstance().format(new Date());

        @Override
        protected Void doInBackground(Void... voids) {
            zadaciBaza.insertZadaci(new Zadaci(0001, "Prvi zadatak", "napravi blabla", "Srednje", "Aktivno", 5, 5, pocetnoVrijeme, zavrsenoVrijeme));
            zadaciBaza.insertZadaci(new Zadaci(0002, "Drugi zadatak", "napravi mnmnmnm", "Tesko", "Neaktivno", 8, 6, pocetnoVrijeme, zavrsenoVrijeme));
            zadaciBaza.insertZadaci(new Zadaci(0003, "Treci zadatak", "napravi xyz", "Tesko", "Aktivno", 9, 7, pocetnoVrijeme, zavrsenoVrijeme));

            zaposleniciBaza.insertZaposlenik(new Zaposlenici(0001, "Darko", "ZZZZ", "FERIT", 1111345690));
            zaposleniciBaza.insertZaposlenik(new Zaposlenici(0002, "Mirko", "XXX", "FERIT", 234532123));
            zaposleniciBaza.insertZaposlenik(new Zaposlenici(0003, "Marko", "YYYY", "FERIT", 653748764));

            return null;
        }
    }


}

