package hratos.praksa.mateojovic.zadatakak15;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ZadaciAdapter extends RecyclerView.Adapter<ZadaciAdapter.ZadaciHolder> {

    private List<Zadaci> zadacis = new ArrayList<>();

    @NonNull
    @Override
    public ZadaciHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.zadaci_item, parent, false);
        return new ZadaciHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ZadaciHolder holder, int position) {
        Zadaci trenutniZadaci = zadacis.get(position);
        holder.textViewTitle.setText(trenutniZadaci.getNaziv());
        holder.textViewDescription.setText(trenutniZadaci.getOpis());
        holder.textViewPriority.setText(String.valueOf(trenutniZadaci.getId()));
    }

    @Override
    public int getItemCount() {
        return zadacis.size();
    }

    public void setZadaciList(List<Zadaci>  zadacis){
        this.zadacis = zadacis;
        notifyDataSetChanged();
    }

    class ZadaciHolder extends RecyclerView.ViewHolder{
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewPriority;

        public ZadaciHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);
        }
    }
}

