package hratos.praksa.mateojovic.zadatakak15;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ZadaciViewModel zadaciViewModel;
    private ZaposleniciViewModel zaposleniciViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);


        final ZaposleniciAdapter adapterZaposlenici = new ZaposleniciAdapter();
        recyclerView.setAdapter(adapterZaposlenici);



        zaposleniciViewModel = new ViewModelProvider(this).get(ZaposleniciViewModel.class);
        zaposleniciViewModel.getSviZaposlenici().observe(this, new Observer<List<Zaposlenici>>() {
            @Override
            public void onChanged(List<Zaposlenici> zaposlenicis) {
                //Toast.makeText(MainActivity.this, "Zaposlenici", Toast.LENGTH_SHORT).show();
                adapterZaposlenici.setZaposleniciList(zaposlenicis);
            }
        });

        RecyclerView recyclerViewZadaci = findViewById(R.id.recycler_view_zadaci);
        recyclerViewZadaci.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewZadaci.setHasFixedSize(true);


        final ZadaciAdapter adapterZadaci = new ZadaciAdapter();
        recyclerViewZadaci.setAdapter(adapterZadaci);


        zadaciViewModel = new ViewModelProvider(this).get(ZadaciViewModel.class);
        zadaciViewModel.getSviZadaci().observe(this, new Observer<List<Zadaci>>() {
            @Override
            public void onChanged(List<Zadaci> zadacis) {
                //Toast.makeText(MainActivity.this, "Zadaci", Toast.LENGTH_SHORT).show();
                adapterZadaci.setZadaciList(zadacis);
            }
        });



    }
}
