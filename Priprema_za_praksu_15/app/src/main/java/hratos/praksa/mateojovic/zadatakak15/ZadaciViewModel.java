package hratos.praksa.mateojovic.zadatakak15;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ZadaciViewModel extends AndroidViewModel {

    private ZadaciRepozitoriji zadaciRepozitoriji;
    private LiveData<List<Zadaci>> sviZadaci;

    public ZadaciViewModel(Application application) {
        super(application);
        zadaciRepozitoriji = new ZadaciRepozitoriji(application);
        sviZadaci = (LiveData<List<Zadaci>>) zadaciRepozitoriji.getZadaciList();
    }


    public void insert(Zadaci zadaci){
        zadaciRepozitoriji.insert(zadaci);
    }

    public void update(Zadaci zadaci){
        zadaciRepozitoriji.update(zadaci);
    }

    public void delete(Zadaci zadaci){
        zadaciRepozitoriji.delete(zadaci);
    }

    public LiveData<List<Zadaci>> getSviZadaci(){
        return sviZadaci;
    }
}
