package hratos.praksa.mateojovic.zadatakak15;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ZaposleniciAdapter extends RecyclerView.Adapter<ZaposleniciAdapter.ZaposleniciHolder> {

    private List<Zaposlenici> zaposlenicis = new ArrayList<>();

    @NonNull
    @Override
    public ZaposleniciHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.zaposlenici_item, parent, false);
        return new ZaposleniciHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ZaposleniciHolder holder, int position) {
        Zaposlenici trenutniZaposlenik = zaposlenicis.get(position);
        holder.textViewTitle.setText(new StringBuilder().append(trenutniZaposlenik.getIme()).append(trenutniZaposlenik.getPrezime()).toString());
        holder.textViewDescription.setText(trenutniZaposlenik.getRadnoMjesto());
        holder.textViewPriority.setText(String.valueOf(trenutniZaposlenik.getId()));

    }

    @Override
    public int getItemCount() {
        return zaposlenicis.size();
    }

    public void setZaposleniciList(List<Zaposlenici> zaposlenicis){
        this.zaposlenicis = zaposlenicis;
        notifyDataSetChanged();
    }

    class ZaposleniciHolder extends RecyclerView.ViewHolder{
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewPriority;

        public ZaposleniciHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);
        }
    }
}
